﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BankSampleAPI.Models
{
    public class AccountEnquiry
    {
        public AccountEnquiry()
        {
        }
    }

    public class AccountEnquiryRequest
    {
        public string account_no { get; set; }
    }

    public class AccountEnquiryResponse : Response
    {
        public IEnumerable<CustomerAccountDetails> getcustomeracctsdetailsresp { get; set; }
    }

    public class CustomerAccountDetails
    {
        public string AccountClassType { get; set; }
        public string accountName { get; set; }
        public string accountNo { get; set; }
        public string AccountOfficer { get; set; }
        public string AccountStatus { get; set; }
        public decimal amountCreditMTD { get; set; }
        public decimal amountCreditYTD { get; set; }
        public decimal amountDebitMTD { get; set; }
        public decimal amountDebitYTD { get; set; }
        public decimal amountHold { get; set; }
        public decimal amountLastCredit { get; set; }
        public decimal amountLastDebit { get; set; }
        public string aTMStatus { get; set; }
        public decimal availableBalance { get; set; }
        public string branchCode { get; set; }
        public string branchName { get; set; }
        public string BVN { get; set; }
        public decimal clearedBalance { get; set; }
        public decimal closingBalance { get; set; }
        public string COMP_MIS_2 { get; set; }
        public string COMP_MIS_4 { get; set; }
        public string COMP_MIS_8 { get; set; }
        public string currency { get; set; }
        public string currencyCode { get; set; }
        public string custID { get; set; }
        public string CustomerCategory { get; set; }
        public string CustomerCategoryDesc { get; set; }
        public string customerName { get; set; }
        public string customerNo { get; set; }
        public string dateofbirth { get; set; }
        public string dateOpened { get; set; }
        public decimal daueLimit { get; set; }
        public string dAUEStartDate { get; set; }
        public string e_mail { get; set; }
        public decimal interestPaidYTD { get; set; }
        public decimal interestReceivedYTD { get; set; }
        public string lastCreditDate { get; set; }
        public decimal lastCreditInterestAccrued { get; set; }
        public string lastDebitDate { get; set; }
        public decimal lastDebitInterestAccrued { get; set; }
        public string lastMaintainedBy { get; set; }
        public int lastSerialofCheque { get; set; }
        public int lastUsedChequeNo { get; set; }
        public string maintenanceAuthorizedBy { get; set; }
        public decimal netBalance { get; set; }
        public decimal oDLimit { get; set; }
        public decimal openingBalance { get; set; }
        public string phone { get; set; }
        public string productCode { get; set; }
        public string productName { get; set; }
        public string profitCenter { get; set; }
        public decimal serviceChargeYTD { get; set; }
        public string signatory { get; set; }
        public string staff { get; set; }
        public string strAdd1 { get; set; }
        public string strAdd2 { get; set; }
        public string strAdd3 { get; set; }
        public string strCity { get; set; }
        public string strCountry { get; set; }
        public string strState { get; set; }
        public string strZip { get; set; }
        public decimal taxAccrued { get; set; }
        public decimal unavailableBalance { get; set; }
        public decimal unclearedBalance { get; set; }
        public string customersegment { get; set; }
        public string PNDReasonANDCode { get; set; }
        public string TierDetails { get; set; }
        public string hasSweep { get; set; }
        public decimal sweepAmt { get; set; }
        public string sweepData { get; set; }
        public string LoanPrequalifyInfo { get; set; }
        public string migrateacctPrompt { get; set; }
        public string gender { get; set; }
        public string custAddress1 { get; set; }
        public string custAddress2 { get; set; }
        public string custAddress3 { get; set; }
        public string custAddress4 { get; set; }
        public string maritalStatus { get; set; }
        public string firstName { get; set; }
        public string middleName { get; set; }
        public string lastName { get; set; }
        public string customerType { get; set; }
    }
}