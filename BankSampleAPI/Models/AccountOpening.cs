﻿using System;
namespace BankSampleAPI.Models
{
    public class AccountOpening
    {
        public AccountOpening()
        {
        }
    }

    public class AccountOpeningRequest
    {
        public string source { get; set; }
        public string app_user_id { get; set; }
        public string app_branch_code { get; set; }
        public string account_branch_code { get; set; }
        public string customer_no { get; set; }
        public string account_class { get; set; }
        public string account_ccycode { get; set; }
        public string account_name { get; set; }
        public string account_onpnd { get; set; }
        public string account_onpostnocr { get; set; }
        public string account_ondormant { get; set; }
        public string account_classtype { get; set; }
        public string account_opendate { get; set; }
        public string account_onfrozen { get; set; }
        public string chequebook_account { get; set; }
        public string atm_account { get; set; }
        public string min_balreqd { get; set; }
        public string salary_account { get; set; }
        public string maker_id { get; set; }
        public string checker_id { get; set; }
        public string auth_stat { get; set; }
        public string comp_mis2 { get; set; }
        public string comp_mis3 { get; set; }
        public string comp_mis4 { get; set; }
    }

    public class AccountOpeningResponse
    {
        public string response_code { get; set; }
        public string response_desc { get; set; }
        public string accountno_created { get; set; }
        public string bo_code { get; set; }
        public string bo_message { get; set; }
    }
}
