﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BankSampleAPI.Models
{
    public class LoanSummary
    {
        public string LoanAccountNumber { get; set; }
        public string CustomerNo { get; set; }
        public string BranchCode { get; set; }
        public string ProductCode { get; set; }
        public string ProductCategory { get; set; }
        public string ProductDescription { get; set; }
        public string CustomerName { get; set; }
        public string DomiciliaryAddress { get; set; }
        public string CustomerAddress { get; set; }
        public DateTime OriginalStartDate { get; set; }
        public DateTime BookDate { get; set; }
        public DateTime ValueDate { get; set; }
        public DateTime MaturityDate { get; set; }
        public string LoanTenor { get; set; }
        public string LoanStatus { get; set; }
        public string CurrencyCode { get; set; }
        public decimal AmountFinanced { get; set; }
        public string ApplicantName { get; set; }
        public decimal TotalPrincipalOutstanding { get; set; }
        public decimal TotalInterestOutstanding { get; set; }
        public decimal PrincipalOverDue { get; set; }
        public decimal PrincipalNotDue { get; set; }
        public decimal InterestNotDue { get; set; }
        public decimal InterestOverDue { get; set; }
        public decimal NextPrincipalDue { get; set; }
        public decimal NextInterestDue { get; set; }
        public decimal PrincipalPaid { get; set; }
        public decimal InterestPaid { get; set; }
        public decimal InterestPenaltyPaid { get; set; }
    }

    public class LoanSummaryByCustomerIDRequest
    {
        public string customer_no { get; set; }
    }

    public class LoanSummaryByCustomerIDResponse : Response
    {
        public IEnumerable<LoanSummary> loanstmtbycustomernoresp { get; set; }
    }
}
