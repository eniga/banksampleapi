﻿using System;
namespace BankSampleAPI.Models
{
    public class PayDayLoan
    {
        public PayDayLoan()
        {
        }
    }

    public class PayDayLoanRequest
    {
        public string request_id { get; set; }
        public string phone_number { get; set; }
        public decimal amount { get; set; }
        public string module_name { get; set; }
        public string transNumber { get; set; }
    }

    public class PayDayLoanResponse
    {
        public string loanref { get; set; }
        public string response_id { get; set; }
        public string request_id { get; set; }
        public string customer_id { get; set; }
        public string loan_accountnumber { get; set; }
        public string phone { get; set; }
        public string currency { get; set; }
        public string amount { get; set; }
        public DateTime disbursement_date { get; set; }
        public string modulename { get; set; }
        public string IsThirdParty { get; set; }
        public string hasOptin { get; set; }
        public bool IsAccessBankCustomer { get; set; }
        public string response_descr { get; set; }
        public string response_code { get; set; }
        public string full_description { get; set; }
    }

    public class PayDayLoanEligibilityRequest
    {
        public string request_id { get; set; }
        public string phone_number { get; set; }
        public string module_name { get; set; }
        public DateTime created_date { get; set; }
    }

    public class PayDayLoanEligibilityResponse
    {
        public string tenor { get; set; }
        public string productName { get; set; }
        public string request_id { get; set; }
        public string amount { get; set; }
        public string lastSalaryDate { get; set; }
        public string response_id { get; set; }
        public string avg_salary { get; set; }
        public string hasOptin { get; set; }
        public string last_sal_amount { get; set; }
        public string sal_account { get; set; }
        public decimal MinimumAmount { get; set; }
        public decimal interest { get; set; }
        public decimal maintenance { get; set; }
        public string module { get; set; }
        public string TotalDelinquentAmount { get; set; }
        public string DelinquentLoanCount { get; set; }
        public string response_descr { get; set; }
        public string response_code { get; set; }
        public string full_description { get; set; }
        public string DisplayMessage { get; set; }
    }
}
