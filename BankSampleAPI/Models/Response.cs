﻿using System;
namespace BankSampleAPI.Models
{
    public class Response
    {
        public string ResponseCode { get; set; }
        public string ResponseMessage { get; set; }
    }
}
