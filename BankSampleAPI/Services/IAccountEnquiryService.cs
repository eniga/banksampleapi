﻿using System;
using BankSampleAPI.Models;

namespace BankSampleAPI.Services
{
    public interface IAccountEnquiryService
    {
        AccountEnquiryResponse GetCustomerAcctsDetailDetail(AccountEnquiryRequest request);
    }
}
