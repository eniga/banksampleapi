﻿using System;
using BankSampleAPI.Models;

namespace BankSampleAPI.Services
{
    public interface IPayDayLoanService
    {
        PayDayLoanResponse RequestLoan(PayDayLoanRequest request);
        PayDayLoanEligibilityResponse FetchEligibility(PayDayLoanEligibilityRequest request);
    }
}
