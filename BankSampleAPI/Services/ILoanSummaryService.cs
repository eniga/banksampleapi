﻿using System;
using BankSampleAPI.Models;

namespace BankSampleAPI.Services
{
    public interface ILoanSummaryService
    {
        LoanSummaryByCustomerIDResponse GetLoanSummaryByCustomerID(LoanSummaryByCustomerIDRequest request);
    }
}
