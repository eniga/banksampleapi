﻿using System;
using BankSampleAPI.Models;

namespace BankSampleAPI.Services
{
    public interface IAccountOpeningService
    {
        AccountOpeningResponse FCUBSCreateCustAccount(AccountOpeningRequest request);
    }
}
