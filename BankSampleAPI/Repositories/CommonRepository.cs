﻿using System;
using BankSampleAPI.Services;
using Microsoft.Extensions.Configuration;

namespace BankSampleAPI.Repositories
{
    public class CommonRepository : ICommonService
    {
        IConfiguration configuration;

        public CommonRepository(IConfiguration _configuration)
        {
            configuration = _configuration;
        }

        public string GetBaseUrl()
        {
            return configuration.GetValue<string>("ApplicationSettings:BaseUrl");
        }
    }
}
