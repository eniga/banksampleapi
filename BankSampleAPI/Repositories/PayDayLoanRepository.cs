﻿using System;
using BankSampleAPI.Models;
using BankSampleAPI.Services;
using Newtonsoft.Json;
using RestSharp;

namespace BankSampleAPI.Repositories
{
    public class PayDayLoanRepository : IPayDayLoanService
    {
        private readonly string BaseUrl;
        ICommonService service;

        public PayDayLoanRepository(ICommonService _service)
        {
            service = _service;
            BaseUrl = service.GetBaseUrl();
        }

        public PayDayLoanEligibilityResponse FetchEligibility(PayDayLoanEligibilityRequest request)
        {
            PayDayLoanEligibilityResponse response = new PayDayLoanEligibilityResponse();
            try
            {
                RestRequest req = new RestRequest(resource: "Paydayloan/v1/paydayloan/FetchEligibility", method: Method.POST);
                RestClient client = new RestClient();
                client.BaseUrl = new Uri(BaseUrl);
                req.AddJsonBody(request);
                IRestResponse result = client.Execute(req);
                if (result.IsSuccessful)
                {
                    response = JsonConvert.DeserializeObject<PayDayLoanEligibilityResponse>(result.Content);
                }
            }
            catch (Exception ex)
            {
                response.response_code = "96";
                response.response_descr = ex.Message;
            }
            return response;
        }

        public PayDayLoanResponse RequestLoan(PayDayLoanRequest request)
        {
            PayDayLoanResponse response = new PayDayLoanResponse();
            try
            {
                RestRequest req = new RestRequest(resource: "Paydayloan/v1/paydayloan/RequestLoan", method: Method.POST);
                RestClient client = new RestClient();
                client.BaseUrl = new Uri(BaseUrl);
                req.AddJsonBody(request);
                IRestResponse result = client.Execute(req);
                if (result.IsSuccessful)
                {
                    response = JsonConvert.DeserializeObject<PayDayLoanResponse>(result.Content);
                }
            }
            catch (Exception ex)
            {
                response.response_code = "96";
                response.response_descr = ex.Message;
            }
            return response;
        }
    }
}
