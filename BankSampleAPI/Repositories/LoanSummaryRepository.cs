﻿using System;
using BankSampleAPI.Models;
using BankSampleAPI.Services;
using Newtonsoft.Json;
using RestSharp;

namespace BankSampleAPI.Repositories
{
    public class LoanSummaryRepository : ILoanSummaryService
    {
        private readonly string BaseUrl;
        ICommonService service;

        public LoanSummaryRepository(ICommonService _service)
        {
            service = _service;
            BaseUrl = service.GetBaseUrl();
        }

        public LoanSummaryByCustomerIDResponse GetLoanSummaryByCustomerID(LoanSummaryByCustomerIDRequest request)
        {
            LoanSummaryByCustomerIDResponse response = new LoanSummaryByCustomerIDResponse();
            try
            {
                RestRequest req = new RestRequest(resource: "AccessBankMaintenancenEnquiry/v1/GetLoanSummaryByCustomerID", method: Method.POST);
                RestClient client = new RestClient();
                client.BaseUrl = new Uri(BaseUrl);
                req.AddJsonBody(request);
                IRestResponse result = client.Execute(req);
                if (result.IsSuccessful)
                {
                    response = JsonConvert.DeserializeObject<LoanSummaryByCustomerIDResponse>(result.Content);
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = "96";
                response.ResponseMessage = ex.Message;
            }
            return response;
        }
    }
}
