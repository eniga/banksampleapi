﻿using System;
using BankSampleAPI.Models;
using BankSampleAPI.Services;
using Newtonsoft.Json;
using RestSharp;

namespace BankSampleAPI.Repositories
{
    public class AccountEnquiryRepository : IAccountEnquiryService
    {
        private readonly string BaseUrl;
        ICommonService service;

        public AccountEnquiryRepository(ICommonService _service)
        {
            service = _service;
            BaseUrl = service.GetBaseUrl();
        }

        public AccountEnquiryResponse GetCustomerAcctsDetailDetail(AccountEnquiryRequest request)
        {
            AccountEnquiryResponse response = new AccountEnquiryResponse();
            try
            {
                RestRequest req = new RestRequest(resource: "AccessBankMaintenancenEnquiry/v1/GetCustomerAcctsDetail", method: Method.POST);
                RestClient client = new RestClient();
                client.BaseUrl = new Uri(BaseUrl);
                req.AddJsonBody(request);
                IRestResponse result = client.Execute(req);
                if (result.IsSuccessful)
                {
                    response = JsonConvert.DeserializeObject<AccountEnquiryResponse>(result.Content);
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = "96";
                response.ResponseMessage = ex.Message;
            }
            return response;
        }
    }
}
