﻿using System;
using BankSampleAPI.Models;
using BankSampleAPI.Services;
using Newtonsoft.Json;
using RestSharp;

namespace BankSampleAPI.Repositories
{
    public class AccountOpeningRepository : IAccountOpeningService
    {
        private readonly string BaseUrl;
        ICommonService service;

        public AccountOpeningRepository(ICommonService _service)
        {
            service = _service;
            BaseUrl = service.GetBaseUrl();
        }

        public AccountOpeningResponse FCUBSCreateCustAccount(AccountOpeningRequest request)
        {
            AccountOpeningResponse response = new AccountOpeningResponse();
            try
            {
                RestRequest req = new RestRequest(resource: "FlexcubeService/v1/FCUBSCreateCustAccount", method: Method.POST);
                RestClient client = new RestClient();
                client.BaseUrl = new Uri(BaseUrl);
                req.AddJsonBody(request);
                IRestResponse result = client.Execute(req);
                if (result.IsSuccessful)
                {
                    response = JsonConvert.DeserializeObject<AccountOpeningResponse>(result.Content);
                }
            }
            catch (Exception ex)
            {
                response.response_code = "96";
                response.response_desc = ex.Message;
            }
            return response;
        }
    }
}
