using System;
using BankSampleAPI.Repositories;
using BankSampleAPI.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace BankSampleAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            // Add functionality to inject IOptions<T>
            services.AddOptions();

            services.AddTransient<IAccountEnquiryService, AccountEnquiryRepository>();
            services.AddTransient<ICommonService, CommonRepository>();
            services.AddTransient<IPayDayLoanService, PayDayLoanRepository>();
            services.AddTransient<ILoanSummaryService, LoanSummaryRepository>();
            services.AddTransient<IAccountOpeningService, AccountOpeningRepository>();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "Access Bank Sample API",
                    Version = "v1",
                    Description = "Access Bank Sample App Middleware Web APIs",
                    TermsOfService = new Uri("https://www.accessbankplc.com/"),
                    Contact = new OpenApiContact
                    {
                        Name = "Access Bank Support",
                        Email = "info@accessbankplc.com"
                    },
                    License = new OpenApiLicense
                    {
                        Name = "Use under Access Bank license permission",
                        Url = new Uri("https://www.accessbankplc.com/")
                    }
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // Enable cors
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
                );

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My Access Bank Sample API V1");
                c.RoutePrefix = string.Empty;
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
