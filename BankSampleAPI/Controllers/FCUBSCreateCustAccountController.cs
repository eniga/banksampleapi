﻿using BankSampleAPI.Models;
using BankSampleAPI.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BankSampleAPI.Controllers
{
    [Route("api/v1/[controller]")]
    public class FCUBSCreateCustAccountController : Controller
    {
        IAccountOpeningService service;

        public FCUBSCreateCustAccountController(IAccountOpeningService _service)
        {
            service = _service;
        }

        // POST api/values
        [HttpPost]
        public AccountOpeningResponse FCUBSCreateCustAccount([FromBody] AccountOpeningRequest request)
        {
            return service.FCUBSCreateCustAccount(request);
        }
    }
}
