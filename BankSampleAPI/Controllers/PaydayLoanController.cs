﻿using BankSampleAPI.Models;
using BankSampleAPI.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BankSampleAPI.Controllers
{
    [Route("api/v1/[controller]")]
    public class PaydayLoanController : Controller
    {
        IPayDayLoanService service;

        public PaydayLoanController(IPayDayLoanService _service)
        {
            service = _service;
        }

        // POST api/values
        [HttpPost("RequestLoan")]
        public PayDayLoanResponse RequestLoan([FromBody] PayDayLoanRequest request)
        {
            return service.RequestLoan(request);
        }


        // POST api/values
        [HttpPost("FetchEligibility")]
        public PayDayLoanEligibilityResponse FetchEligibility([FromBody] PayDayLoanEligibilityRequest request)
        {
            return service.FetchEligibility(request);
        }
    }
}
