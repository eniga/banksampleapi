﻿using BankSampleAPI.Models;
using BankSampleAPI.Repositories;
using BankSampleAPI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BankSampleAPI.Controllers
{
    [Route("api/v1/[controller]")]
    public class GetCustomerAcctsDetailDetailController : Controller
    {
        IAccountEnquiryService service;

        public GetCustomerAcctsDetailDetailController(IAccountEnquiryService _service)
        {
            service = _service;
        }

        // POST api/values
        [HttpPost]
        public AccountEnquiryResponse Post([FromBody] AccountEnquiryRequest request)
        {
            return service.GetCustomerAcctsDetailDetail(request);
        }
    }
}
