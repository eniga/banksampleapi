﻿using BankSampleAPI.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BankSampleAPI.Controllers
{
    [Route("api/v1/[controller]")]
    public class GetLoanSummaryByCustomerIDController : Controller
    {
        // POST api/values
        [HttpPost]
        public LoanSummaryByCustomerIDResponse Post([FromBody] LoanSummaryByCustomerIDRequest request)
        {
            return new LoanSummaryByCustomerIDResponse();
        }
    }
}
